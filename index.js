
// Global Objects
// Arrays

let students = [
	"Tony", 
	"Peter", 
	"Wanda", 
	"Vision", 
	"Loki"];
console.log(students);

// push()
// is an array method that we use to ADD an item at the END of an array
students.push("Thor");
console.log(students);

// unshift()
// is an array method that we use to ADD an item at the START of an array
students.unshift("Steve");
console.log(students);

// pop()
// is an array method that we use to DELETE an item at the END of an array
students.pop();
console.log(students);

// shift()
// is an array method that we use to DELETE an item at the START of an array
students.shift();
console.log(students);

// MUTATOR METHOD
// splice()
// is an array method that REMOVES and ADDS items from a STARTING INDEX of an array 

// NON-MUTATOR METHOD
// slice()
// is an array method that COPIES a portion from a STARTING INDEX and RETURNS a new array from it

// ITERATOR METHOD - loops over the items of an array
// forEach()
// loops over items in an array and repeats a user-defined function

// map()
// loops over items in an array and repeats a user-defined function and RETURNS in a NEW ARRAY

// every()
// loops and checks if all items satisfy a given condition returns a boolean value


let arrNum = [15,20,25,30,11]
console.log(arrNum);
// check if every item in arrNum is divisible by 5

let allDivisible;

arrNum.forEach(num =>{
	if(num % 5 === 0){
		console.log(`${num} is divisible by 5`);
	}
	else{
		allDivisible = false;
	}
	// However, can forEach() return data that will tell us IF all numbers / items in our arrNum array is divisible by 5
})
console.log(allDivisible);

arrNum.pop();
arrNum.push(35);
console.log(arrNum);

let divisibleBy5 = arrNum.every(num =>{
	console.log(num);
	return num % 5 === 0;
})
console.log(divisibleBy5); // returns TRUE

// Math
// mathematical constants
console.log(Math);
console.log(Math.E); // Euler's number
console.log(Math.PI); // PI
console.log(Math.SQRT2); // square root of 2
console.log(Math.SQRT1_2); // square root of 1/2
console.log(Math.LN2); // natural logarithm of 2
console.log(Math.LN10); // natural logarithm of 10
console.log(Math.LOG2E); // base 2 logarithm of E
console.log(Math.LOG10E); // base 10 logarithm of E

// methods for rounding a number to an integer
console.log(Math.round(Math.PI)); // rounds to a nearest integer result : 3
console.log(Math.ceil(Math.PI)); // rounds to a nearest integer result : 4
console.log(Math.floor(Math.PI)); // rounds to a nearest integer result : 3
console.log(Math.trunc(Math.PI)); // rounds to a nearest integer result : 3

// method for returning the square root of a number
console.log(Math.sqrt(3.14)); // rounds to a nearest integer result : 1.77
console.log(Math.min(-4,-3,-2,-1,0,1,2,3,4)); //returns the lowest value
console.log(Math.max(-4,-3,-2,-1,0,1,2,3,4));// returns the max value

/*
=================================================================================
A C T I V I T Y  S O L U T I O N:
=================================================================================
*/
// 1. Create a function named addToEnd that will add a passed in string to the end of a passed in array. If element to be added is not a string, return the string "error - can only add strings to an array". Otherwise, return the updated array. Use the students array and the string "Ryan" as arguments when testing.


let arr = ["John", "Joe", "Jane", "Jessie"];
/*
function addToEnd(students){
	if(typeof(students) === "string"){
	arrName.push(students);
		console.log(arrName);
	}
	else{
		console.log("error - can only add String");
	}
}

*/

const addToEnd = (arr, element) =>{
	if(typeof element != "string"){
		return "error - can only add strings to an array";
	}
	arr.push(element);
	return arr;
}

// test input
console.log(addToEnd(students, "Ryan"))
// validation check
console.log(addToEnd(students, 45))


// 2. Create a function named addToStart that will add a passed in string to the start of a passed in array. If element to be added is not a string, return the string "error - can only add strings to an array". Otherwise, return the updated array. Use the students array and the string "Tess" as arguments when testing.


/*
function addToStart(students){
	if(typeof(students) === "string"){
	arrName.unshift(students);
		console.log(arrName);
	}
	else{
		console.log("error - can only add String");
	}
}
*/
const addToStart = (arr, element) =>{
	if(typeof element != "string"){
		return "error - can only add strings to an array";
	}
	arr.unshift(element);
	return arr;
}

// test input
console.log(addToStart(students, "June"))
// validation check
console.log(addToStart(students, 45))

// 3. Create a function named elementChecker that will check a passed in array if at least one of its elements has the same value as a passed in argument. If array is empty, return the message "error - passed in array is empty". Otherwise, return a boolean value depending on the result of the check. Use the students array and the string "Jane" as arguments when testing.



/*function elementChecker(students){
	if(students===null || students==="" || students===undefined){
		console.log("array is empty");
	}
	else{
		for(i = arrName.length - 1; i >= 0; i--) {
			
			if(arrName[i] === students){
				return true;
			}
		}
		return false;
	}
}*/

const elementChecker = (arr, elementToBeChecked)=>{
	if(arr.length ===0){
		return "error - passed in array is empty"
	}
	return arr.some(element => element === elementToBeChecked)
}

console.log(elementChecker(arr,"Jane"))
console.log(elementChecker([],"Jane"))

// 4. Create a function named checkAllStringsEnding that will accept the passed  array and a character. The function will do the ff:

/*
	if array is empty, return "error - array must NOT be empty"
	if at least one array element is NOT a string, return "error - all array elements must be strings"
	if 2nd argument is NOT of data type string, return "error - 2nd argument must be of data type string"
	if 2nd argument is more than 1 character long, return "error - 2nd argument must be a single character"
	if every element in the array ends in the passed character, return true. Otherwise return false.

Use the students array and the character "e" as arguments when testing.

*/



/*function checkAllStringEnding(var01, var02){
	if(var01.length === 0){
		console.log("error - array must not be empty")
	}
	else{
		for(i=var01.length-1; i>=0; i--){
			if(typeof(var01[i]) !== "string"){
				return "error - All elements must be STRING"
			}
			else{
				if(typeof(var02) !== "string"){
					return "error - 2nd argument must be STRING"		
				}
				else{
					if(var02.length > 1){
						return "error 2nd argument must be a single character"
					}
					else{
						return true;
					}
				}
			}
		}
	}
}
*/

const checkAllStringsEnding = (arr, char) =>{
	if(arr.length === 0) return "error - array must NOT be empty";

	if(arr.some(element => typeof element !== "string")) return "array elements must be string";

	if(typeof char !== "string") return "error - 2nd argument must be String"

	if(char.length > 1) return "error - 2nd argument must be a single character"

	return arr.every(element => element[element.length - 1] === char);
}

console.log(checkAllStringsEnding(students, "e"));
console.log(checkAllStringsEnding([],"e"));
console.log(checkAllStringsEnding("Jane",2));


// 5. Create a function named stringLengthSorter that will take in an array of strings as its argument and sort its elements in an ascending order based on their lengths. If at least one element is not a string, return "error - all array elements must be strings". Otherwise, return the sorted array. Use the students array to test.
const stringLengthSorter = (arr) => {
    if(arr.some(element => typeof element !== "string")) return "error - all array elements must be strings";
    arr.sort((elementA, elementB) => {
        return elementA.length - elementB.length;
    })
    return arr;
}
console.log("Question 5:")
//test input
console.log(stringLengthSorter(students)); //['Tess', 'Tony', 'Loki', 'Ryan', 'Peter', 'Wanda', 'Vision']
//validation check
console.log(stringLengthSorter([37, "John", 39, "Jane"])); //"error - all array elements must be strings"
